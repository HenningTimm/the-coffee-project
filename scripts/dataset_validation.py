import argparse
import pathlib
import pandas as pd
import sys
import glob
import os
import numpy as np
from pandas_schema import Column, Schema
from pandas_schema.validation import (
    InRangeValidation,
    DateFormatValidation,
    MatchesPatternValidation,
    LeadingWhitespaceValidation,
    TrailingWhitespaceValidation,
    IsDtypeValidation,
)


def main(args):
    def string_validators():
        """Create default validators for string columns"""
        return [LeadingWhitespaceValidation(), TrailingWhitespaceValidation()]

    validators = [
        # Assure that the index is a positive integer
        Column("index", [InRangeValidation(min=0), IsDtypeValidation(np.integer)]),
        # Assure that weights are floats and within a reasonable range
        Column("weight", [InRangeValidation(min=0.0, max=1.0)]),
        # Prevent leading and trailing white spaces in coffee names
        # (and other strings)
        Column("coffee_name", string_validators()),
        # Assure batch numbers are positive integers
        Column("batch", [InRangeValidation(min=0), IsDtypeValidation(np.integer)]),
        # Assure dates are of the form YYYY-MM-DD. For details, see:
        # https://docs.python.org/3/library/datetime.html
        Column("roasting_date", [DateFormatValidation(date_format="%Y-%m-%d")]),
        Column("roaster", string_validators()),
        Column("aroma_1", string_validators()),
        Column("aroma_2", string_validators()),
        Column("aroma_3", string_validators()),
        Column("country", string_validators()),
        Column("region", string_validators()),
        # Assure heights are integers between 0 and 8000,
        # but allow NAN's for missing height values.
        Column(
            "height_min",
            [InRangeValidation(min=0, max=8000) | MatchesPatternValidation("nan")],
        ),
        Column(
            "height_max",
            [InRangeValidation(min=0, max=8000) | MatchesPatternValidation("nan")],
        ),
        Column("species", string_validators()),
        Column("variety", string_validators()),
        Column("process", string_validators()),
        Column("decaf", string_validators()),
        # Assure that scale ids adhere to UUID standards
        Column(
            "scale_id",
            [
                MatchesPatternValidation(
                    pattern="^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$"
                )
            ],
        ),
        # Assure that ORCID ids adhere to ORDCID format.
        # This does not validate the checksum.
        Column(
            "contributor_orcid",
            [
                MatchesPatternValidation(
                    pattern="^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9]|X$"
                )
            ],
        ),
        # Assure dates are of the form YYYY-MM-DD.
        Column("measurement_date", [DateFormatValidation(date_format="%Y-%m-%d")]),
        # Assure total bag weight is a positive float,
        # assuming bags weigh at least 125g.
        Column("bag_total_weight", [InRangeValidation(min=125.0)]),
        # Assure remaining bag weight is a positive float
        Column("bag_remaining_weight", [InRangeValidation(min=0.0)]),
    ]
    schema = Schema(validators)

    if os.path.isfile(args.path):
        csv_data_files = [args.path]
    else:
        csv_data_files = list(glob.glob(os.path.join(args.path, "*.csv")))

    errors = False
    for filename in csv_data_files:
        # read in the file and rename the first (unnamed) column to index
        result_data = pd.read_csv(filename)
        result_data = result_data.rename(columns={"Unnamed: 0": "index"})

        # Apply the schema. If there are errors, print each of them.
        for error in schema.validate(result_data):
            errors = True
            print(f"Invalid entry for Sample in file {filename} \n{error}\n")

    # If any error occurred for any column, exit with an error code.
    # If no error occurred, this finishes without error.
    if errors:
        sys.exit("Could not add results. Some files contained errors.")
    elif not csv_data_files:
        sys.exit(
            f"No files to check. Check the input path is supplied correctly.\nFound path: {args.path}"
        )
    else:
        print("All checks passed!")
        checked_files = (
            "\n".join(csv_data_files) if len(csv_data_files) > 1 else csv_data_files[0]
        )
        print("Checked_files:\n{}".format(checked_files))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Validate csv files using pandas_schema."
    )
    parser.add_argument(
        "path",
        type=pathlib.Path,
        help="Path to a csv file or to a data folder. If you supply a folder, all csv files in this folder and its sub-folders will be checked.",
    )
    args = parser.parse_args()
    main(args)
