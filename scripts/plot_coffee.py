import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


# def average_weights_by_(df, filename):
#     """Plot a histogram of bean weights split up by coffee name.
#     Also create a KDE for each coffee.
#     """
#     weight_by_name = sns.displot(
#         df,  # Data source
#         x="weight",  # variable used for the x-axis of the histogram
#         hue="coffee_name",  # variable used for coloring
#         kde=True,  # plot KDE in addition to bars
#         multiple="layer",  # layer bars semi transparently
#         binwidth=0.01,  # use the rounded precision of input data as bin size
#         height=10.0,  # figure height in inches
#         aspect=1.5,  # figure aspect ratio
#     )

#     # Adapt labels
#     weight_by_name.ax.set_title("Weight distribution by roast (coffee name)")
#     weight_by_name.ax.set_xlabel("Weight [grams]")
#     weight_by_name.ax.set_ylabel("Number of measurements")

#     # Adjust white space around figure
#     plt.tight_layout()

#     # Plot figure to file. File type is selected through the
#     # variable output_type in the main function
#     plt.savefig(filename)


def weight_histogram_total(df, filename):
    """Plot a histogram of all bean weights and a singular KDE."""
    weight_total = sns.displot(
        df,  # Data source
        x="weight",  # variable used for the x-axis of the histogram
        kde=True,  # plot KDE in addition to bars
        binwidth=0.01,  # use the rounded precision of input data as bin size
        height=10.0,  # figure height in inches
        aspect=1.5,  # figure aspect ratio
    )

    # Adapt labels
    weight_total.ax.set_title("Total weight distribution")
    weight_total.ax.set_xlabel("Weight [grams]")
    weight_total.ax.set_ylabel("Number of measurements")

    # Adjust white space around figure
    plt.tight_layout()

    # Plot figure to file. File type is selected through the
    # variable output_type in the main function
    plt.savefig(filename)


def weight_violins_by_roast(df, filename):
    """Plot weight distribution for each coffee roast"""

    fig, ax = plt.subplots(
        figsize=(20.0, 10.0),  # define figure dimensions
    )
    ax.tick_params(axis="x", rotation=-45)

    # create a new column for sample sizes
    df["sample_size"] = 0

    # Compute the mean weight per group and count sample sizes
    average_weights = (
        df.groupby("coffee_name")
        .aggregate({"weight": "mean", "sample_size": "count"})
        .reset_index()
    )
    # remove sample_size column so we can merge this in later
    df = df.drop("sample_size", axis=1)

    # Rename the weight columns to reflect that it now contains mean values
    average_weights = average_weights.rename(columns={"weight": "mean_weight"})

    # merge mean weight and sample size into the data
    df = pd.merge(df, average_weights, on="coffee_name")

    # Sort rows in ascending order of mean weights.
    # This is used in the plot below to order the violins
    average_weights = average_weights.sort_values("mean_weight")

    weight_distr_by_name = sns.violinplot(
        data=df,  # Data source
        x="coffee_name",  # Axis used to group values into violins
        y="weight",  # Variable used to generate the violins
        # Sort groups/ violins by mean weight in ascending order
        order=list(average_weights["coffee_name"]),
    )
    # Adapt labels
    weight_distr_by_name.set_title("Weight distribution by coffee name")
    ax.set_xlabel("Coffee Name (Roast)")
    ax.set_ylabel("Weight [grams]")

    # Add sample sizes to x tick labels
    x_tick_labels = []
    for label in ax.get_xticklabels():
        # Get coffee names from existing labels
        c_name = label.get_text()

        # extract sample sizes saved for this coffee
        sample_sizes = df.loc[df.coffee_name == c_name]["sample_size"]

        # Assert that all entries have the same value.
        # This is a safety measure to be certain that the minimum
        # we use below is representative for the coffee.
        assert min(sample_sizes) == max(sample_sizes)
        sample_size = min(sample_sizes)

        # Format a new tick label by appending the sample size
        # to the old tick label extracted earlier
        x_tick_labels.append(f"{c_name}\n n={sample_size}")

    # Use the modified tick labels
    ax.set_xticklabels(x_tick_labels)

    # reduce figure clutter and adjust white spaces
    sns.despine()
    plt.tight_layout()

    # Plot figure to file. File type is selected through the
    # variable output_type in the main function
    plt.savefig(filename)


if __name__ == "__main__":
    sns.set(
        style="whitegrid",
    )

    # Read the csv file supplied through the snakemake rule
    # via the input field. (See: Snakefile)
    # Make sure the saved index col is recognized.
    df = pd.read_csv(snakemake.input.coffee_csv, index_col=0)
    # Round weights to the second decimal
    df = df.round({"weight": 2})

    weight_violins_by_roast(df, filename=snakemake.output.violins)
    weight_histogram_total(df, filename=snakemake.output.total_weight_distribution)
