import pandas as pd
import glob

# Read the path to the raw data files supplied through the snakemake rule
# via the params field. (See: Snakefile)
data_sets = [
    pd.read_csv(csv_file, index_col=0)  # Read each csv file into a pandas DataFrame
    for csv_file in glob.glob(
        f"{snakemake.params.raw_data_folder}/*.csv"
    )  # Visit all csv files in the given folder
]

# Concatenate all collected data frames and create a unified index variable for them
concatenated_coffee_data = pd.concat(data_sets, ignore_index=True)

# Write the concatenated data frames as a csv file to the path specified
# in the output filed of the Snakemake rule (See: Snakefile)
concatenated_coffee_data.to_csv(snakemake.output.coffee_csv)
